# Commands:
#   hubot liste les <n> derniers <entity> - Affiche les <n> derniers <entity> depuis le referential

module.exports = (robot) ->
  robot.respond /liste les (.*) derniers (.*)/i , (res) ->
    limit = res.match[1]
    entity = res.match[2]
    allowedEntities = ["orders","ordres","ordre","k"];
    if entity in allowedEntities
      pool = require('../lib/referential.db')
      if entity in ["orders","ordres","ordre"]
        sqltable = "orders_table"
      else
        sqltable = "koncentrator"
      pool.query "select * from #{sqltable} order by id desc limit $1::int", [limit], (err, response) ->
        if err
          res.send "Aïe !!! "+ err;
        else if sqltable is "orders_table"
          table = "| Id | Kname | Content |\n"
          table += "|----|-------|---------|"
          for order ,i in response.rows
            table += "\n| "+order.id+" | "+order.kname+" | "+order.content+" |"
          res.send table;
        else
          table = "| Id | Kname |\n"
          table += "|----|-------|"
          for koncentrator ,i in response.rows
            table += "\n| "+koncentrator.id+" | "+koncentrator.name+" |"
          res.send table;
    else
      res.send "Je ne connais pas l'entité #{entity} :("
