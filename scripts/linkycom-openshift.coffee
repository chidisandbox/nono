# Commands:
#   hubot status <environment> - Affiche le statut d'un environment sur Openshift
#   hubot routes <projet> sur <environment> - Affiche les routes disponibles pour <projet> sur l'environment <environment>

module.exports = (robot) ->
  robot.hear /linkycom/i, (res) ->
    res.send "Linkycom? ça roxx du poney"

  robot.hear /nono\?/i, (res) ->
    res.send "http://lemontageenimage.l.e.pic.centerblog.net/b25fcaac.gif"
    res.send "Yo !!! Je peux t'aider ? "

  robot.respond /status (.*)/i , (res) ->
    env = res.match[1]
    @login = require('child_process').exec
    logincmd = "oc login https://openshift.linky.io:8443  -u chidi -p singes && oc project linkycom-#{env}"
    res.send "Bouge pas je t'envoie ça ..."
    @login logincmd, (error, stdout, stderr) ->
      if error
        res.send error
        res.send stderr
      else
        statusCmd = "oc status"
        @status = require('child_process').exec
        @status statusCmd, (error, stdout, stderr) ->
          if error
            res.send error
            res.send stderr
          else
            res.send "```\n"+stdout+"```"

  robot.respond /routes (.*) sur (.*)/i , (res) ->
    project = res.match[1]
    env = res.match[2]
    @login = require('child_process').exec
    logincmd = "oc login https://openshift.linky.io:8443  -u chidi -p singes && oc project linkycom-#{env}"
    res.send "Bouge pas je t'envoie ça ..."
    @login logincmd, (error, stdout, stderr) ->
      if error
        res.send error
        res.send stderr
      else
        routesCmd = "oc get routes -l app=#{project}"
        @routes = require('child_process').exec
        @routes routesCmd, (error, stdout, stderr) ->
          if error
            res.send error
            res.send stderr
          else
            res.send "```\n"+stdout+"```"
